<?php
namespace vendor\pillax\arr\src;

/**
 * Array helper
 */
class Arr {

    /**
     * Return true when $arr is multidimensional
     *
     * @param array $arr
     * @return bool
     */
    public static function isMulti(array $arr) : bool {
        return count($arr) != count($arr, COUNT_RECURSIVE);
    }

    /**
     * Get element from array by key, return $default when element don't exists
     *
     * @param array $arr
     * @param mixed $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public static function get(array $arr, $key, $default=null) {
        return isset($arr[$key]) ? $arr[$key] : $default;
    }

}